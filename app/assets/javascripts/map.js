

var pos = null;


$(document).on("ready", function() {
    console.log("ADS");

    function initialize() {
 
                        var styles = [
                            {
                                "stylers": [
                                    {
                                        "hue": "#baf4c4"
                                    },
                                    {
                                        "saturation": 10
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "stylers": [
                                    {
                                        "color": "#effefd"
                                    }
                                ]
                            },
                            {
                                "featureType": "all",
                                "elementType": "labels",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "administrative",
                                "elementType": "labels",
                                "stylers": [
                                    {
                                        "visibility": "on"
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            }
                        ];
 
                        // Estilo para el mapa.
                        var styledMap = new google.maps.StyledMapType(styles,
                        {name: "Styled Map"});
                        
                
                        var mapOptions = {
                               scaleControl: true,
                                zoom: 15
                            }


                        map = new google.maps.Map(document.getElementById('map-canvas'),
                                mapOptions);

                        var infowindow = new google.maps.InfoWindow();

                        var mymarker = new google.maps.Marker({
                            position: null,
                            map: null,
                            scale: 1,
                            title: "My point"
                            //title: data.name + ", " + data.country
                            });

                        //var GeoMarker = new GeolocationMarker(map);
                        
                        setInterval(function(){
                        // Try HTML5 geolocation
                          if(navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(function(position) {

                              pos = new google.maps.LatLng(position.coords.latitude,
                                                               position.coords.longitude);
                                /*
                                map.setCenter(pos);
                                */
                                mymarker.setMap(null);
                                mymarker = new google.maps.Marker({
                                position: pos,
                                map: map,
                                scale: 1,
                                title: "Yo",
                                zIndex: 999,
                                //icon: '/assets/user_mark.png'
                                //title: data.name + ", " + data.country

                                icon: new google.maps.MarkerImage('//maps.gstatic.com/mapfiles/mobile/mobileimgs2.png',
                                                new google.maps.Size(22,22),
                                                new google.maps.Point(0,18),
                                                new google.maps.Point(11,11))

                                });
                                mymarker.setMap(map);
/*
                                google.maps.event.addListener(mymarker, 'click', (function(mymarker, key) {
                                    return function() {
                                        //infowindow.setContent(data.name + ", " + data.country);
                                        //infowindow.setContent(mymarker.title);
                                        
                                                        if( !user_data[5] ){
                                                            infowindow.setContent(' <img id=thub_info class="thumbnail" src=/assets/logo_2.png>'+user_data[2]);
                                                        }
                                                        else{
                                                           infowindow.setContent(' <img id=thub_info class="thumbnail" src=' + user_data[5] + '>'+user_data[2]);
                                                       }

                                        infowindow.open(map, mymarker);
                                    }
                                })(mymarker, i));
                               */ 

                            }, function() {
                              handleNoGeolocation(true);
                            });

                          } else {
                            // Browser doesn't support Geolocation
                            handleNoGeolocation(false);
                          }

                          function handleNoGeolocation(errorFlag) {
                              if (errorFlag) {
                                var content = 'Error: The Geolocation service failed.';
                              } else {
                                var content = 'Error: Your browser doesn\'t support geolocation.';
                              }
                            }
                        }, 5000);
                         
                        //PUNTO INICIAL
                        if(navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(function(position) {

                              var posi = new google.maps.LatLng(position.coords.latitude,
                                                               position.coords.longitude);
                                
                                map.setCenter(posi);

                            }, function() {
                              handleNoGeolocation(true);
                            });

                          } else {
                            // Browser doesn't support Geolocation
                            handleNoGeolocation(false);
                          }


                          //INFO USER
                          /*
                          var user_data = [];
                          var user_id = document.getElementById("id_user");
                          console.log(user_id.textContent);

                          $.get("/users/"+user_id.textContent+".json", function(json1) {
                            $.each(json1, function(key, data) {
                                
                                user_data.push(data);
                                
                            });
                            console.log(user_data);

                          });
                          */
 
                        var marker, i;

                                $.get("/plants.json", function(json1) {
                                        console.log(json1);
                                    $.each(json1, function(key, data) {
                                        var latLng = new google.maps.LatLng(data.latitude, data.longitude);
                                        // Creo un marcador y lo pongo en el mapa.
                                        var marker = new google.maps.Marker({
                                            position: latLng,
                                            map: map,
                                            scale: 1,
                                            title: data.name,
                                            icon: '/assets/tree_mark.png'
                                            //title: data.name + ", " + data.country
                                        });
                                        marker.setMap(map);
                                        
                                        //var im = String(data.images);
                                        //console.log("ruta="+im);

                                        google.maps.event.addListener(marker, 'click', (function(marker, key) {
                                                return function() {
                                                        if( !data.images[0] ){
                                                            infowindow.setContent(' <img id=thub_info class="thumbnail" src=/assets/logo_2.png> <a href="/plants/'+ data.id +'">'+ data.name +'</a>' +
                                                                                    '<p>'+ data.description +'</p>');
                                                        }
                                                        else{
                                                            //infowindow.setContent(' <img id=thub_info class="thumbnail" src=  /public/uploads/plants/0_Arbol Salitre 1.jpg > <a href="/plants/'+ data.id +'">'+ data.name +'</a>');
                                                            infowindow.setContent(' <img id=thub_info class="thumbnail" src=/assets/logo_2.png> <a href="/plants/'+ data.id +'">'+ data.name +'</a>');
                                                        }

                                                        infowindow.open(map, marker);
                                                }
                                                })(marker, i));
                                    });
                                });


 
                        map.mapTypes.set('map_style', styledMap);
                        map.setMapTypeId('map_style');
                }
 
                $(window).on('load', function(event) {
                        event.preventDefault();
                        initialize();
                        google.maps.event.addDomListener(window, 'load', initialize);
                });

});