

var suiche=true;
var pos = null;
var contador = 0;




//$('#refrescar').click(function() {
//    location.reload();
//});


$(document).on("ready", function() {


        //LOADING BOTON BUSCAR PLANTAS
    var encuentra = setInterval(function(){
        
        //$('.glyphicon-repeat').hide();
        //$('#buscar_btn').show();

        if(pos != null){
            $('#buscar_btn').show();
            $('#buscar_btn').html("Buscar plantas cercanas");
            $('#err_aviso').hide();
            $('#buscar_btn').addClass("btn btn-success");
            clearInterval(encuentra);
            contador = 0;
        }
        else{

            if (contador >= 10){
                $('#err_aviso').html("No pudimos encontrarte :c, habilita las opciones de localizacion y recarga la vista");
                $('#buscar_btn').removeClass( "btn btn-warning" );
                //$('#buscar_btn').hide();
                $('#err_aviso').addClass("alert alert-danger");
                $('#cont_refre').addClass("btn btn-warning");
                var dir = document.createElement('a');
                dir.id = "Refrescar";
                dir.href = "javascript:history.go(0)";
                //dir.className = "btn btn-danger";
                var node = document.createTextNode("Recargar");
                $('.glyphicon-repeat').show();

                //$('#refrescar').addClass("glyphicon glyphicon-repeat");

                dir.appendChild(node);
                $('#cont_refre').append(dir);
                clearInterval(encuentra);
            }
            else{
                contador++;
                console.log(contador);
                $('#err_aviso').html("Espera, estamos buscandote");
                //$('#buscar_btn').removeClass( "btn btn-success" );
                $('#err_aviso').addClass("alert alert-info");
            }
        }

    }, 1000);

    //location.reload();
        // Mi posición
    if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {

      pos = new google.maps.LatLng(position.coords.latitude,
                                       position.coords.longitude);

    }, function() {
      handleNoGeolocation(true);
    });

    } else {
    console.log("NO ENCONTRADO");
    // Browser doesn't support Geolocation
    handleNoGeolocation(false);
    }

    function handleNoGeolocation(errorFlag) {
      if (errorFlag) {
        var content = 'Error: The Geolocation service failed.';
      } else {
        var content = 'Error: Your browser doesn\'t support geolocation.';
      }
    }


    $('.glyphicon-repeat').hide();
    $('#buscar_btn').hide();
    // Codigo a ejecutar cuando el html esté listo.
    $('.modal_director').on('click', function(event) {
        event.preventDefault();
        var actor = $(this).attr('href');

        suiche=!suiche;

        if(suiche){
            $(actor).show(function() {
                //$(actor + ' .modal_content').slideDown();
                //$(actor + ' .modal_content').fadeIn();
                $('.modal_actor').animate({width:'toggle'},350);
            });
        }
        else{
            $('.modal_actor').animate({width:'toggle'},350);
            //$('.modal_actor').fadeOut();
        }
    });

    //console.log(suiche);

    $('.modal_close').on('click', function() {
       // $('.modal_actor').hide();
       $('.modal_actor').fadeOut();
    });
    $('.modal_actor').on('click', function() {
        //$(this).hide();
        $(this).fadeOut();
    });
    /*
    $('.modal_content').on('click', function(event) {
        event.stopPropagation();
    });
    */

    var mensaje_b = setInterval(function(){
        $('.alert-warning').fadeOut("normal");
        clearInterval(mensaje_b);
    }, 2000);


    $('#buscar_btn').on('click', function() {

            // Mi posición
      if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {

          pos = new google.maps.LatLng(position.coords.latitude,
                                           position.coords.longitude);

        }, function() {
          handleNoGeolocation(true);
        });

      } else {
        console.log("NO ENCONTRADO");
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
      }

      function handleNoGeolocation(errorFlag) {
          if (errorFlag) {
            var content = 'Error: The Geolocation service failed.';
          } else {
            var content = 'Error: Your browser doesn\'t support geolocation.';
          }
        }

        var cercas = 0;

        $("#lista_a").empty("slow");
        //console.log(pos);

        $.get("/plants.json", function(json1) {
            
            $.each(json1, function(key, data) {
                
                //console.log(pos);

                if (pos != null){

                    var pos_array = $.map(pos, function(value, index) {
                        return [value];
                    });
                    
                    //console.log(array);

                    var Dist = getDistanceFromLatLonInKm( pos_array[0], pos_array[1], data.latitude , data.longitude)*1000;
                    //console.log("DISTANCIA ARBOL # "+data.id+": "+Dist + "Pos:" + pos);
                    var toType  = tipo(pos);

                    

                    if( Dist < 2000 ){
                        
                        cercas++;

                        var para = document.createElement("li");
                        para.className = "list-group-item list-group-item-success"
                        var dir = document.createElement('a');
                        dir.id = "n_planta";
                        dir.href = '/plants/'+ data.id;
                        para.appendChild(dir);
                        var node = document.createTextNode(data.name);
                        dir.appendChild(node);
                        var element = document.getElementById("lista_a");
                        element.appendChild(para);
                        
                        //var container = document.getElementById("container");
                        //container.innerHTML = '<span style="color:red;">Hello</span>';
                    }

                }
            });
            //console.log(cercas);
            if(cercas < 1) {
                var para = document.createElement("li");
                para.className = "list-group-item list-group-item-danger"
                var node = document.createTextNode("No hay plantas cerca :c");
                para.appendChild(node);
                var element = document.getElementById("lista_a");
                element.appendChild(para);  
            }
        });
    });

});


function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI/180)
}

var TYPES = {
    'undefined'        : 'undefined',
    'number'           : 'number',
    'boolean'          : 'boolean',
    'string'           : 'string',
    '[object Function]': 'function',
    '[object RegExp]'  : 'regexp',
    '[object Array]'   : 'array',
    '[object Date]'    : 'date',
    '[object Error]'   : 'error'
},
TOSTRING = Object.prototype.toString;

function tipo(o) {
    return TYPES[typeof o] || TYPES[TOSTRING.call(o)] || (o ? 'object' : 'null');
};