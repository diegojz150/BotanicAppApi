class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception


  def index


    if session[:user]
        @user = User.find(session[:user])
        redirect_to user_path(@user, format: :json)
        #respond_to do |format|
          #format.json { render json: @user }
          #format.xml { render xml: @user }
        #end
    else
        #render json: "No logueado."
        #render "/index"
        respond_to do |format|
          format.json { render json: "No logueado."}
          #format.xml { render xml: "No logueado." }
          format.html { render "/index" }
        end
    end
    

  end

  def email
  	ClienteMailer.email_verification(params[:name], params[:email]).deliver_now
  	puts "Se envio el correo."
  	#redirect_to "/"
    respond_to do |format|
      format.json { render json: "Email Sent." }
    end
  end
  

  def language
    if params[:short] && params[:short] == "es"
      I18n.default_locale, I18n.locale = :es, :es
    else
      I18n.default_locale, I18n.locale = :es, :en
    end
    redirect_to "/"
  end

  def showmap
    @user = User.find(session[:user]) if session[:user]
    cookies[:user] = @user.id
    render "/map"
  end


end
