class UsersController < ApplicationController

  $vista = 0

  def index #Todos los usuarios

    @user = User.find(session[:user]) if session[:user]

    if  @user.role < 2
      $vista = 1

      @users = User.all
      #render "index"
      respond_to do |format|
        format.json {render json: @users}
        format.html
      end

    end

  end

  # Vista del perfil de usuario
  def show

    @us = User.find(session[:user])

    if @us.role < 2
      $vista = 0
      @user = User.find(params[:id])
      puts "EL ID ES=" + params[:id]
      puts "LA SESION ES=" + session[:user].to_s

      respond_to do |format|
        format.json {render json: @user}
        format.html
      end

    else
      if session[:user].to_s == params[:id]
        
        $vista = 0
        @user = User.find(params[:id])
        puts "EL ID ES=" + params[:id]
        puts "LA SESION ES=" + session[:user].to_s

        respond_to do |format|
          format.json {render json: @user}
          format.html
        end

      end

    end

  end


  # Vista para editar.
  def edit

    @user = User.find(params[:id]) if session[:user]
    respond_to do |format|
      format.json {render json: @user}
      format.html
    end

  end

  def update
    @user = User.find(params[:id])

    if !@user.provider_uid
      @user.password = Digest::SHA1.hexdigest(user_params[:password])
    end


    if params[:user][:image] && !params[:user][:image].is_a?(String)
      if @user.image && Dir.glob("public#{@user.id}_#{@user.name}").count > 0
        File.delete("public#{@user.id}_#{@user.name}")
      end
      #@user.image = "/uploads/users/#{Random.new_seed.to_s}.#{params[:user][:image].original_filename.split(".").last}"
      @user.image = "/uploads/users/#{@user.id}_#{@user.name}.#{params[:user][:image].original_filename.split(".").last}"
      File.open('public' + @user.image, "wb") do |f|
        f.write(params[:user][:image].read)
      end
    end

    params[:user][:password] = @user.password
    params[:user][:image] = @user.image

    if @user.update(user_params)
        
        redirect_to user_path(@user, format: :json)

        #if $vista == 1
        #  redirect_to users_path
        #else
        #  redirect_to @user
        #end

    else
      render "perfil"
    end

  end

  def changerole
    @user = User.find(params[:id])
    @user.role = user_params[:role]

    if @user.save
      redirect_to users_path
    end

  end

  def favoritos 
    @user = User.find(session[:user]) if session[:user]
    @plants = Plant.all
    @plants_user = []

    @plants.each do |plant|
      if plant.favs.include?(@user.id) 
       @plants_user.push( plant )
      end
    end

      respond_to do |format|
        format.json {render json: @plants_user}
        format.html
      end

  end

  # Método para guardar datos del usuario nuevo.
  def create
    #params.except!( :password_c )
    #puts "CLAVESINHA: "+params[:password_c].to_s

    @user = User.new(user_params)
    @user.uuid = Digest::SHA1.hexdigest(@user.id.to_s)
    @user.estado=1
    @user.role=2

   # pass = params[:user][:password]
   # puts "CLAVESINHA:" + pass
   # puts "CLAVESINHA2:" + params[:user][:password]

    if params[:user][:image] && !params[:user][:image].is_a?(String)
      if @user.image && Dir.glob("public#{@user.id}_#{@user.name}").count > 0
        File.delete("public#{@user.id}_#{@user.name}")
      end
      #@user.image = "/uploads/users/#{Random.new_seed.to_s}.#{params[:user][:image].original_filename.split(".").last}"
      @user.image = "/uploads/users/#{@user.id}_#{@user.name}.#{params[:user][:image].original_filename.split(".").last}"
      File.open('public' + @user.image, "wb") do |f|
        f.write(params[:user][:image].read)
      end
    end
    
    if @user.save

      if !@user.provider_uid
      #session[:user] = @user.id
      #@url = "http://localhost:3000/verifica/" + @user.id.to_s
      @url = request.protocol + request.host_with_port + "/verifica/" + @user.uuid
      puts "URL ACT:" + @url
      ClienteMailer.email_verification(@user.name, @user.email, @url).deliver_now

      
        #redirect_to user_path(@user)
      flash[:notice] = 'Se ha enviado un correo a tu cuenta, por favor confirma tu correo'
      redirect_to root_path
    else
      session[:user] = @user.id
      #redirect_to user_path(@user)
      redirect_to user_path(@user, format: :json)
    end

    elsif params[:user][:provider_uid]
      #@user.errors = nil
      render "tw_new"
    else
      render "new"
    end

  end

  def new
  # Esto debería ser el formulario de creación de usuarios.
  #render json: params[:data], content_type: "application/json"    
  end


    # Método para guardar datos de facebook, twitter.
  def connect
    if env["omniauth.auth"]
      @user = User.where(provider_uid: env["omniauth.auth"]["uid"]).first
      unless @user
        @user = User.new
        @user.name = env["omniauth.auth"]["extra"]["raw_info"]["name"]
        @user.email = env["omniauth.auth"]["extra"]["raw_info"]["email"]
        @user.image = env["omniauth.auth"]["info"]["image"]
        @user.gender = env["omniauth.auth"]["extra"]["raw_info"]["gender"]
        @user.verified = env["omniauth.auth"]["extra"]["raw_info"]["verified"]
        @user.provider_uid = env["omniauth.auth"]["uid"]
        @user.username = env["omniauth.auth"]["uid"]
        @user.role = 2
        @user.estado = 1

        @user.uuid = Digest::SHA1.hexdigest(@user.id.to_s)

        #puts env["omniauth.auth"]["extra"]["raw_info"]["name"].to_s
        #puts env["omniauth.auth"]["extra"]["raw_info"]["email"].to_s
        #puts env["omniauth.auth"]["extra"]["raw_info"]["gender"].to_s
        #puts env["omniauth.auth"]["extra"]["raw_info"]["verified"].to_s
        #puts env["omniauth.auth"]["uid"].to_s
=begin        
        if env["omniauth.auth"]["extra"]["raw_info"]["email"]
          @user.username = env["omniauth.auth"]["uid"]
        else
          @user.username = env["omniauth.auth"]["uid"]["nickname"]
        end
=end
        if @user.save
            session[:user] = @user.id
            #redirect_to user_path(@user)
            redirect_to user_path(@user, format: :json)

            #render json: @user, content_type: "application/json"
            #render json: env["omniauth.auth"], content_type: "application/json"
        else
          #if @user.email
            
            #render json: env["omniauth.auth"], content_type: "application/json"
            #flash[:notice] = 'Ya existes'
            #render "/index"

          #else
            
            #@user.username = env["omniauth.auth"]["info"]["nickname"]
            if env["omniauth.auth"]["extra"]["raw_info"]["email"]
              flash[:notice] = @user.username
            end

            @user.errors.clear
            #render "tw_new"
            respond_to do |format|
              format.json {render json: "Error"} #env["omniauth.auth"]["extra"]["raw_info"]["email"]
              format.html {render "tw_new"}
            end

            #render json: env["omniauth.auth"], content_type: "application/json"
          #end
        end

      else
        if @user.estado != 0
          session[:user] = @user.id
          flash[:notice] = 'Logueado satisfactoriamente'
          #redirect_to user_path(@user)
          redirect_to user_path(@user, format: :json)
        else
          flash[:notice] = 'Inactivo viejo'
          redirect_to root_path(format: :json)
        end
      end
    elsif params[:error]
      render plain: "#{params[:error]} #{params[:error_reason]}", content_type: "application/plain"
    else
      render plain: "No se pudo conectar con #{params[:provider]}", content_type: "application/plain"
    end
  end

  def login
    user = User.where(username: user_params[:username]).first

    input_pass = Digest::SHA1.hexdigest(user_params[:password])
    #input_pass = user_params[:password]

    #puts "PP:" + user_params[:password]
    #puts "passesC: "+ user.password + "_" + input_pass

    if user
      user.forgot = nil

      if user.save

        if user.verified

          if user.estado == 1

            if !user.provider_uid

                if user.password == input_pass
                  session[:user] = user.id.to_s
                  flash[:notice] = 'Logueado satisfactoriamente'
                  redirect_to user_path(user, format: :json)
                else
                  #flash[:notice] = "Correo o contraseña incorrectos"
                  flash[:notice] = t :wrongpass
                  render "/index"
                end
            else
              flash[:notice] = t :wrongsocial
              render "/index"
            end

          else

              if user.password == input_pass
                user.image = nil
                user.estado = 1
                session[:user] = user.id.to_s

                plants = Plant.all

                if plants
                  for i in 0...plants.length
                    if plants[i].favs.include?(user.id)
                      plants[i].favs.delete(user.id)
                    end
                    if plants[i].save
                      puts "BORRADO FAVORITO DE " + plants[i].name.to_s
                    end
                  end
                end
                
                if user.save
                  flash[:notice] = "Bienvenido de nuevo"
                 redirect_to user_path(user, format: :json)
                end

              else
                flash[:notice] = "Usuario o contraseña incorrectos"
                #flash[:notice] = "Nonono"
                render "/index"
              end

          end
        else
          flash[:notice] = 'Primero confirma tu correo'
          render "/index"
        end

      else
        flash[:notice] = "Usuario o contraseña incorrectos"
        #flash[:notice] = t :wrongpass
        render "/index"
      end
    else
        flash[:notice] = "Usuario o contraseña incorrectos"
        #flash[:notice] = t :wrongpass
        render "/index"
    end

  end

  def signout
    session[:user]=nil
    redirect_to "/"
  end

  def inactive
    @user = User.find(session[:user])
    @user.estado = 0

    #render "/index"
    if @user.update(user_params)
      redirect_to "/index"
      session[:user]=nil
    else
      render "perfil"
    end
  end

  def email_verification

    #user = User.find(params[:uuid])
    user = User.where( uuid: params[:uuid] ).first

    user.verified = true

    if user.save
        flash[:notice] = "Tu cuenta ha sido activada, por favor logueate"
        #session[:user] = user.id
        #redirect_to user_path(user)
        redirect_to root_path
      else
        flash[:notice] = "Tu cuenta NO se ha podido activar"
        render "/index"
    end

  end

  def forgot_pass
    render "/forgot"
    #render "forgot_change"
  end

  def forgot_check

    time1 = Time.new
    user = User.where(email: user_params[:email]).first
    #user.forgot = time1.sec.to_s + time1.min.to_s + time1.hour.to_s + time1.day.to_s + time1.month.to_s + time1.year.to_s

    if user
      if !user.provider_uid
            user.forgot = time1.hour.to_s + time1.day.to_s + time1.month.to_s + time1.year.to_s
        if user.save
          #flash[:notice] = "El correo si existe"
          #render "/forgot"
          #url = "http://localhost:3000/forgot/" + user.uuid

          url = request.protocol + request.host_with_port + "/forgot/" + user.uuid
          ClienteMailer.email_forgot(user.name, user.email, url).deliver_now
          flash[:notice] = "Se te ha enviado un correo para cambiar de pass"
          redirect_to root_path
        end
      else
        flash[:notice] = "El correo está registrado con Facebook o Twitter"
        render "/forgot"
      end
    else
      flash[:notice] = "El correo no existe"
      render "/forgot"
    end

  end

  def forgot_change
    time1 = Time.new
    @user = User.where(uuid: params[:uuid]).first
    #puts "PARAMETRO="+params[:uuid]
    #@user = User.find(1)
    
    #actual_forgot = time1.sec.to_s + time1.min.to_s + time1.hour.to_s + time1.day.to_s + time1.month.to_s + time1.year.to_s
    actual_forgot = time1.hour.to_s + time1.day.to_s + time1.month.to_s + time1.year.to_s

    if @user.forgot == actual_forgot
      render "forgot_change"
    else
      flash[:notice] = "Errorseins, se vencio pide otra vez"
      redirect_to root_path
    end
  end

  def forgot_success
    
    @user = User.where(uuid: params[:uuid]).first
    
    puts "TAM:"+ params[:user][:password].length

    if params[:user][:password] != "" || params[:user][:password].length > 5

      @user.password = Digest::SHA1.hexdigest(params[:user][:password])

      params[:user][:password] = @user.password

        if @user.update(user_params)
          flash[:notice] = "Ha sido cambiada tu pass"
          redirect_to root_path
        else
          flash[:notice] = "Errorseins"
          render "forgot_change"
        end
    else
      flash[:notice] = "Escribe una contraseña valida"
      render "forgot_change"
    end

  end

  def destroy

    if session[:user] == params[:id]
      @user = User.find(params[:id])
      @user.destroy
      redirect_to root_path
      session[:user]=nil
    else
      @user = User.find(params[:id])
      @user.destroy
      redirect_to users_path     
    end

  end

  def activacionadmin
    @user = User.find(params[:id])

    #if @user

      if @user.estado == 1
        @user.estado = 0
      else
        @user.estado = 1
      end

      if @user.save
        redirect_to users_path(format: :json)
      end

    #end

  end

  private
  def user_params
    params.require(:user).permit(:uuid ,:name, :username, :email, :image, :gender, :password, :provider_uid, :role, :estado)
    #PARAMS_TO_SCRUB = [ :password_c ]
    #params.require(:user).permit!.except(:password_c)
  end

end