class Plant < ActiveRecord::Base

	validates :common_name, :resume,:description,:distribution,:etnobotanic, presence: true
	validates :name, presence: true, uniqueness: true
	validates :latitude, :longitude, presence: true, uniqueness: true, :numericality => {:only_float => true}

	#Si es a muchos
	has_many :users

	#Si es a uno
	#belongs_to :user
end
