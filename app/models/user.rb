class User < ActiveRecord::Base

	before_create :set_password, unless: :provider_uid?
	
	#validates :name, :gender, :image, presence: true
	validates :username, presence: true, on: :create, uniqueness: true, format: { with: /\A[a-zA-Z0-9]+\Z/ }, length: { minimum: 4 }
	validates :name, :gender, :role, :estado, presence: true, on: :create
	validates :email, presence: true, uniqueness: true, format: {with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i}
	#validates :password, presence: true, length: { minimum: 6 }, unless: :provider_uid?, on: :forgot_success
	validates :password, presence: true, length: { minimum: 6 }, unless: :provider_uid?

	belongs_to :plant

	# Este método te asignará la encriptación solo si el campo es valido
	# Es decir si el campo tiene mínimo 6 caracteres.
	def set_password
      if self.valid?
      	self.password = Digest::SHA1.hexdigest(self.password)
      end
	end

end