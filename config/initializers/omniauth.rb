Rails.application.config.middleware.use OmniAuth::Builder do
	provider :facebook, ENV["FB_ID"], ENV["FB_SECRET"], :image_size => "large"
	provider :twitter, ENV["TW_ID"], ENV["TW_SECRET"],
    {
      :image_size => 'original'
    }
	#Si falla la conexión.
	on_failure { |env| UsersController.action(:connect).call(env) }
end

#, {:client_options => {:ssl => {:ca_path => "/etc/ssl/certs"}}},