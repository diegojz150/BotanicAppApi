class CreatePlants < ActiveRecord::Migration
  def change
    create_table :plants do |t|
      t.string :name
      t.string :common_name
      t.string :resume      
      t.string :description      
      t.string :distribution      
      t.string :etnobotanic 
      t.float :latitude
      t.float :longitude
      t.integer :favs, array: true, default: []
      t.string :images, array: true, default: []

      #relacion
      t.references :user, index: true
      t.timestamps null: false
    end
  end

end
